# Change Log

## 0.06
- fixed Skybox Geometry parameters
- Added new snippets
  - Meshes
    - `Sphere`

## 0.05
- Added new snippets
  - Lights
    - Attributes
      - `castShadow`
      - `intensity`
      - `shadowMapSize`
  - Meshes
    - `Plane`
    - `Ring`
- Material Props 
  - side: better description
  - true/false values now with selectable options
  - uses description from docs now

## 0.04
- Fixed missing `"` on light attributes
- Fixed types of attributes lights

## 0.03
- Removed Texture from Materials and gave it its own snippet
- Added new snippets
  - Attributes
    - `color`
  - Meshes
    - `Group`
    - `Box`
  - Materials
    - `LambertMaterial`
    - `PhongMaterial`
    - `ShaderMaterial`
    - `StandardMaterial`
    - Material Props
      - `depthTest`
      - `depthWrite`
      - `flatShading`
      - `fog`
      - `opacity`
      - `side`
      - `transparent`
      - `vertexColors`
  - Textures
      - `Texture`

## 0.02

- Added new snippets
  - `HemisphereLight`
  - `PointLight`
  - `RectAreaLight`
  - `SpotLight`
- Formatted Readme.md
- Added Logo

## 0.01

- Initial release
# TroisJS-Snippets

These snippets were built to improve your workflow when setting up a new or working on an existing [TroisJS](https://troisjs.github.io/) project.

Included are the pieces I got tired of typing over and over again, plus a boilerplate that is helpful to start implementing your next idea right away.


They are based on common use and official documentation. I am __not affiliated__ with the makers of TroisJS, this is an independent project, an attempt to give back to the community.
 
🌊  Adrian 

![overview](images/troisjs.gif)

# Snippets

## Boilerplate

| Command | Description |
| --- | --- |
|`troisjs`| Boilerplate to get you started|
|`skytjs`| Creates skydome, ready for your 360º texture|


## Attributes

|  |  |
| --- | --- |
|`.color`| Color attribute|
|`:pos`| position|
|`:prop` | empty props array|
|`:rot`| rotation|
|`:scale`| scale|


## Meshes
|  |  |
| --- | --- |
|`boxtjs`| Box Mesh|
|`gltfjs`| GLTF Model|
|`group`| Mesh Group|
|`planetjs`| Plane Mesh|
|`ringtjs`| Ring Mesh|


## Lights
|  |  |
| --- | --- |
|`ambl`| AmbientLight|
|`dirl`| DirectionalLight|
|`heml`| HemisphereLight|
|`pointl`| PointLIght|
|`rectl`| RectAreaLight|
|`spotl`| SpotLight|

### Lights Common Props
|  |  |
| --- | --- |
|`.castShadow`| castShadow prop |
|`.intensity`| intensity prop |
|`.shadowMapSize`| shadowMapSize prop |


## Materials
|  |  |
| --- | --- |
|`matbasic`| Basic Material|
|`matlamb`| Mesh Lambert Material|
|`matphong`| Phong Material|
|`matshader`| Shader Material|
|`matstandard`| Standard Material|

### Material Props
|  |  |
| --- | --- |
|`.depthtest`| depthTest prop |
|`.depthwrite`| depthWrite prop |
|`.flatshading`| flatShading prop |
|`.fog`| fog prop |
|`.opacity`| opacity prop |
|`.side`| side prop |
|`.transparent`| transparent prop |
|`.vertexcolors`| vertexColors prop |

## Textures
|  |  |
| --- | --- |
|`tex`| Empty texture element|


---

## Release Notes
See Changelog

## License
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">TroisJS VSCode Snippets</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://onewavestudios.com" property="cc:attributionName" rel="cc:attributionURL">oneWave Studios</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/onewave-public/troisjs-snippets" rel="dct:source">https://gitlab.com/onewave-public/troisjs-snippets</a>.
